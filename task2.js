
import http from 'http';

let server = http.createServer();

server.on('request',(request,response) => {

    response.writeHead(200, {
        'Content-type': 'text/html; charset=utf-8'
    })

    response.end('<h1>Hello Node!!!')
})

server.listen(3000);