import fs from 'fs';
import http from 'http';

let server = http.createServer();

server.on('request',(request,response) => {

    //Task 3
    fs.appendFile('welcome.txt', 'Hello Node', (err) => {
        if(err) throw err;
    })

    fs.readFile('hello.txt','utf-8',(err,data) => {
        if(err) throw err;
        console.log(data);
    })


    //Task 1 & 2
    response.writeHead(200, {
        'Content-type': 'text/html; charset=utf-8'
    })

    response.end('<h1>Hello Node!!!')
    //console.log('hello world');
})

server.listen(3000);