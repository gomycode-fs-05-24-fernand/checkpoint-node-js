import fs from 'fs';
import http from 'http';

let server = http.createServer();

server.on('request',(request,response) => {

    //Task 3
    fs.writeFile('welcome.txt', 'Hello Node', (err) => {
        if(err) throw err;
    })

    fs.readFile('hello.txt','utf-8',(err,data) => {
        
        if(err) throw err;
        console.log(data);
    })
})

server.listen(3000);